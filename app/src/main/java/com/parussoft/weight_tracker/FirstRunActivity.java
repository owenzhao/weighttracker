package com.parussoft.weight_tracker;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioGroup;

public class FirstRunActivity extends Activity {
  private static final String STEP = "Step";

  private int step = 0;
  private Button previousButton;
  private Button nextButton;
  private FrameLayout container;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_first_run);

    container = (FrameLayout) findViewById(R.id.container);
    if (savedInstanceState == null) {
      addFragment();
    }
    else { step = savedInstanceState.getInt(STEP); }

    previousButton = (Button) findViewById(R.id.previousButton);
    nextButton = (Button) findViewById(R.id.nextButton);
    adjustButtonState();
    previousButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        hideFragment();
        --step;
        unHideFragment();
        adjustButtonState();
      }
    });
    nextButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (step == 3) {
          Intent activityIntent = new Intent(getApplicationContext(),
              WeightTrackerActivity.class);
          activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
          | Intent.FLAG_ACTIVITY_CLEAR_TASK);
          activityIntent.putExtra(WeightTrackerActivity.RUN_START_WIZARD, false);

          startActivity(activityIntent);
        }
        else if (container.getChildCount() - 1 > step) {
          hideFragment();
          ++step;
          unHideFragment();
        }
        else {
          hideFragment();
          ++step;
          addFragment();
          adjustButtonState();
        }
      }
    });
  }

  private void hideFragment() {
    container.getChildAt(step).setVisibility(View.INVISIBLE);
  }

  private void unHideFragment() {
    container.getChildAt(step).setVisibility(View.VISIBLE);
  }

  private void addFragment() {
    getFragmentManager().beginTransaction()
        .add(R.id.container, PlaceholderFragment.newInstance(step))
        .commit();
  }

  private void adjustButtonState() {
    previousButton.setEnabled(step != 0);
    nextButton.setText(step != 4 ? R.string.next : R.string.finish);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putInt(STEP, step);

    super.onSaveInstanceState(outState);
  }

  /**
   * A placeholder fragment containing a simple view.
   */
  public static class PlaceholderFragment extends Fragment {
    private static final String LAYOUT = "Layout";
    private static final String VISIBLE = "visible";
    private static final String UNITS = "units";

    private int layout = 0;
    private RadioGroup radioGroup = null;

    public PlaceholderFragment() {
    }

    public static PlaceholderFragment newInstance(int step) {
      PlaceholderFragment fragment = new PlaceholderFragment();

      switch (step) {
        case 0:
          fragment.layout = R.layout.fragment_first_run_00_welcome;
          break;
        case 1:
          fragment.layout = R.layout.fragment_first_run_01_choose_units;
          break;
        case 2:
          fragment.layout = R.layout.fragment_first_run_02_get_personal_information;
          break;
        default:
          fragment.layout = R.layout.fragment_first_run_03_calculate_bmi_and_show_warning;
      }

      return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      int visible = View.VISIBLE;
      if (savedInstanceState != null) {
        layout = savedInstanceState.getInt(LAYOUT);
        visible = savedInstanceState.getInt(VISIBLE);
      }
      View rootView = inflater.inflate(layout, container, false);
      rootView.setVisibility(visible);
      switch (layout) {
        case R.layout.fragment_first_run_01_choose_units:
          radioGroup = (RadioGroup) rootView.findViewById(R.id.unitsGroup);
          if (savedInstanceState != null) {
            radioGroup.check(savedInstanceState.getInt(UNITS));
          }
          break;
        case R.layout.fragment_first_run_02_get_personal_information:

          break;
      }


      return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
      outState.putInt(LAYOUT, layout);
      outState.putInt(VISIBLE, this.getView().getVisibility());

      switch (layout) {
        case R.layout.fragment_first_run_01_choose_units:
          outState.putInt(UNITS, radioGroup.getCheckedRadioButtonId());
          break;
        case R.layout.fragment_first_run_02_get_personal_information:

          break;
      }

      super.onSaveInstanceState(outState);
    }
  }
}
