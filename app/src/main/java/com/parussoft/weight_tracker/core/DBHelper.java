package com.parussoft.weight_tracker.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.parussoft.weight_tracker.core.DBConst.*;

/**
 * Created by 肇鑫 (Owen Zhao) on 14-1-27.
 */
public class DBHelper extends SQLiteOpenHelper {
  private static final String DATABASE_NAME = "weight.db";
  private static final int DATABASE_VERSION = 1;
  private static final String sql = "create table if not exists " + TABLE_NAME +
      " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
      INPUT_TIME + " NUMERIC NOT NULL, " +
      WEIGHT + " NUMERIC NOT NULL)";

  public DBHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(sql);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

  }
}
