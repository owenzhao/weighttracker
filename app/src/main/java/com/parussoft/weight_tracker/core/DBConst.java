package com.parussoft.weight_tracker.core;

/**
 * Created by 肇鑫 (Owen Zhao) on 14-1-27.
 */
public class DBConst {
  private DBConst() {} //Do not allow instantiate this class

  public static final String TABLE_NAME = "weight_table";
  public static final String ID = "id";
  public static final String INPUT_TIME = "input_time";
  public static final String WEIGHT = "weight";
}
